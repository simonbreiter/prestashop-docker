# README

A simple blueprint to quickly setup a prestashop project with docker.

## How to use
This repository is intend to be a starting point for a quick new shop. To start developing fork this project. 

### How to use docker
Start docker container with `./up`. If you want to shutdown your container use `./down`. 

### How to install Prestashop
To install prestashop you can either use the script `./install_prestashop` or
download the ZIP-File manually and unzip it in the folder `prestashop`. You can now install
Prestashop as usual via the browser. This container needs a Link to a Docker-Container
with mysql. The default link is `mysql-db`, change if needed.